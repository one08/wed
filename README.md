# wedionic app



## TODO

- ✅ adding icons for the app (icons + splash screen)
- ✅ fix correct distance showing from nearby restaurants
- ⬇️ auto update distance with position change)
- ✅ Correct reviews to show ( based on what reviews the user gave)
- ✅ Chat- fix the user id in firebase so that we can add a vendor and chat with them
- ✅ Chat -  FixUI , must have blue ticks for read message,message to auto scroll to bottom
- ✅ Chat- Correct profile image to show for vendor
- ⬇️ Chat-allow user to clear messages
- ✅ Change all purple color to - #2c4f6f ( this is for the whole app)
- ✅ Frogot password screen must have heading forgot password and user must enter email and this must work
- ✅ Register screen, user can add image or take picture for profile picture
- ⬇️ Menu, the profile image and name must be from when user did signup
- ⬇️ Home list screen,make sure all shortcut buttons work correctly
- ✅ Find Vendor by caterory ( caterogy must come from firebase)
- ⬇️ Favourite to list places that user has press hear ( favourite on)
- ⬇️ Checklist- Fix UI and functions
- ⬇️ Guestlist- Fix UI and functions
- ⬇️ Fix Reviews UI and functions,to work correctly
- ⬇️ Fix settings screen (jus add location on or off)
- ⬇️ Change logout icon
- ⬇️ Make sure map works in showing places
- ⬇️ Overall fix any bugs and anything that breaks
- ⬇️ Admin panel needs little edits as well
- ⬇️ Need you to send apk after we test latest commited code

