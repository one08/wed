import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { ChatPage } from '../chat/chat';
import { HomePage } from '../home/home';
import firebase from 'firebase/app';
import { Observable } from "rxjs/Observable";
import { AngularFireDatabase } from "angularfire2/database";
import { ChatVendorPage } from "../chat-vendor/chat-vendor";
import { ListPage } from '../list/list';
import { Auth } from '../../providers/auth';
import * as _ from "lodash";
import { AngularFireAuth } from 'angularfire2/auth';
import { ActionSheetController } from 'ionic-angular';
/**
 * Generated class for the ChattingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-chatting',
    templateUrl: 'chatting.html',
})
export class ChattingPage {

    public fireAuth: any;
    user: any;
    rooms: Observable<any[]>;
    chatRooms: any = [];
    userProfile: any;
    fromList: any;
    chatRoomsRef: any;
    constructor(public nav: NavController, public auth: Auth,
        public navParams: NavParams, public afDb: AngularFireDatabase,
        private nativeStorage: NativeStorage, public afAuth: AngularFireAuth
        , public actionSheetCtrl: ActionSheetController) {
        this.fromList = this.navParams.get('fromList');
    }

    afterViewLoad(data) {
        this.afAuth.user
            .subscribe((_user) => {
                this.userProfile = _user;
                console.log(_user);
                this.user = _user
                this.chatRoom();

            })


    }


    chatRoom() {
        this.afDb.object("/chatrooms").valueChanges().subscribe(value => {
            console.log("snap rooms => ", value);
            this.chatRooms = [];
            if (this.user) {
                for (var key in value) {
                    var singleRoom = value[key];
                    if (singleRoom.vendor_id == this.user.uid) {
                        if (singleRoom.last_msg_timestamp) {
                            this.chatRooms.push(singleRoom);
                        }
                    }
                }
                // this.chatRooms.sort((c,r)=>new Date(r.last_msg_timestamp)-new Date(c.last_msg_timestamp))
                this.chatRooms.sort((c, r) => {
                    let firstdate = new Date(r.last_msg_timestamp);
                    let seconddate = new Date(r.last_msg_timestamp);
                    if (firstdate > seconddate) return -1;
                    else if (firstdate < seconddate) return -1;
                    else return 0;


                })
                console.log(this.chatRooms);
            }

        });

    }
    ionViewDidLoad() {

        this.fireAuth = firebase.auth();
        console.log('ionViewDidLoad ChattingPage');

        //    this.nativeStorage.getItem('userProfile')
        //     .then(
        //         data => this.afterViewLoad(data),
        //         error => console.error(error)
        //     );
        let data = this.auth.storingdata;
        console.log("kiransir", data);

        this.afterViewLoad(data)
    }
    capatlize(name: any): string {
        return _.capitalize(name);
    }
    openChat(data) {

        console.log(data);
        this.nav.push(ChatVendorPage, { restaurant: data });
    }
    ChattingBack() {
        console.log("Test")
        this.nav.setRoot(ListPage, { user: this.userProfile });
    }
    logOut() {
        this.fireAuth.signOut();
        console.log('logged out');
        this.nav.setRoot(HomePage);
    }
    showActionSheet(room) {
        console.log(room);
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Clear your messages',
            buttons: [

                {
                    text: 'clear messages',
                    icon: "trash",
                    handler: () => {

                        firebase.database().ref("/chatmessages/" + room.vendor_id + ":" + room.user_id)
                            .remove().then(err => {

                            })
                        firebase.database().ref("/chatrooms/" + room.vendor_id + ":" + room.user_id)
                            .update({ 'last_message': '' }).then(err => {

                            })

                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });

        actionSheet.present();
    }


}
