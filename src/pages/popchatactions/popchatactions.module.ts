import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopchatactionsPage } from './popchatactions';

@NgModule({
  declarations: [
    PopchatactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PopchatactionsPage),
  ],
})
export class PopchatactionsPageModule {}
