import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import 'rxjs/add/operator/filter';


import {Observable} from 'rxjs/Observable';







/*
  Generated class for the LocationTrackerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationTrackerProvider {

	public watch: any;
	public lat: any ;
	public lng: any ;
	public timestamp: any;
	userList: any;

	constructor(public zone: NgZone, public http: HttpClient, 
		public backgroundGeolocation: BackgroundGeolocation,
		 public geolocation: Geolocation) {
// this.lat=Observable.create(observer => {
// 	this.lat = observer;
// });
// this.lng=Observable.create(observer => {
// 	this.lng = observer;
// });

	}


	startTracking() {
 
		// Background Tracking
	   
		let config = {
		  desiredAccuracy: 0,
		  stationaryRadius: 20,
		  distanceFilter: 10,
		  debug: true,
		  interval: 2000
		};
	   console.log("start tracking######");
		this.backgroundGeolocation.configure(config).subscribe((location) => {
	   
		  console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);
	   
		  // Run update inside of Angular's zone
		  this.zone.run(() => {
			this.lat = location.latitude;
			this.lng = location.longitude;
		  });
	   
		}, (err) => {
	   
		  console.log(err);
	   
		});
	   
		// Turn ON the background-geolocation system.
		this.backgroundGeolocation.start();
	   
	   
		// Foreground Tracking
	   
	  let options = {
		frequency: 3000,
		enableHighAccuracy: true
	  };
	   
	  this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {
	   
		//console.log(position);
	   
		// Run update inside of Angular's zone
		this.zone.run(() => {
		  this.lat = position.coords.latitude;
		  this.lng = position.coords.longitude;
		});
	   
	  });
	   
	  }





	stopTracking() {
		console.log('stopTracking');

		this.backgroundGeolocation.finish();
		this.watch.unsubscribe();

	}


	/**
	 *  calculate distance using Haversine Formula
	 *  passing restaurantlist  as arguments
	 * 
	 * @param restaurant 
	 * @returns  Arrays of restaurant with distance key
	 */
	applyHaversine(restaurantlist) {

		let usersLocation = {
			lat: this.lat,
			lng: this.lng
		};
	

		restaurantlist.map((location) => {

			let placeLocation = {
				lat: location.lat,
				lng: location.long
			};

			location.distance = this.getDistanceBetweenPoints(
				{
					lat: this.lat,
					lng: this.lng
				},
				placeLocation,
				'km'
			).toFixed(2);
			return location;
		});

		return restaurantlist;
	}

	/**
	 * Gets distance between points
	 * 
	 * @param start 
	 * @param end 
	 * @param units 
	 * @returns  
	 */
	getDistanceBetweenPoints(start, end, units) {

		let earthRadius = {
			miles: 3958.8,
			km: 6371
		};

		let R = earthRadius[units || 'miles'];
		let lat1 = start.lat;
		let lon1 = start.lng;
		let lat2 = end.lat;
		let lon2 = end.lng;

		let dLat = this.toRad((lat2 - lat1));
		let dLon = this.toRad((lon2 - lon1));
		let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
			Math.sin(dLon / 2) *
			Math.sin(dLon / 2);
		let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		let d = R * c;

		return d;

	}


	/**
	 * To rad
	 * @param x 
	 * @returns  
	 */
	toRad(x) {
		return x * Math.PI / 180;
	}


}
