
	
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
 

 
@Injectable()
export class StorageProvider {
 USER_PROFILE = 'userprofile';
 USER_LOGGED_IN="loggedin";
 IS_VENDOR="isvendor";
  constructor(public storage: Storage) { 


  }
  setIsVendor(isvendor):Promise<any>{
    return this.storage.set(this.IS_VENDOR,isvendor);
  }

  getIsVendor():Promise<any>{
      return new Promise((resolve, reject) => {
        this.storage.get(this.IS_VENDOR).then((val)=>{
            resolve(val);
        });
      });
     
  }
  
  setLoggedIn(loggedin):Promise<any>{
    return this.storage.set(this.USER_LOGGED_IN,loggedin);
  }

  getLoggedIn():Promise<any>{
      return new Promise((resolve, reject) => {
        this.storage.get(this.USER_LOGGED_IN).then((val)=>{
            resolve(val);
        });
      });
     
  }
  setUserProfile(userprofile):Promise<any>{
    return this.storage.set(this.USER_PROFILE,userprofile);
  }

  getUserProfile():Promise<any>{
      return new Promise((resolve, reject) => {
        this.storage.get(this.USER_PROFILE).then((val)=>{
            resolve(val);
        });
      });
     
  }


}